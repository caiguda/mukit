//
//  MUBOCompoundModel.m
//  MUKit
//
//  Created by Malaar on 11.02.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "MUBOCompoundModel.h"

@implementation MUBOCompoundModel

#pragma mark - Init

- (id)initWithModels:(NSArray*)aModels
{
    self = [super init];
    if(self)
    {
        _models = [NSArray arrayWithArray:aModels];
        _maxModelsCount = 1;
    }
    return self;
}

#pragma mark - 

+ (NSArray*)compoundModelsFromModels:(NSArray*)aModels
                        groupedByCount:(NSUInteger)aGroupCount
{
    NSMutableArray* result = [NSMutableArray array];

    MUBOCompoundModel* hub;
    NSArray* subarray;
    int count = aModels.count;
    
    for(int i = 0; i < count; i += aGroupCount)
    {
        subarray = [aModels subarrayWithRange:NSMakeRange(i, MIN(count - i, aGroupCount))];
        hub = [[MUBOCompoundModel alloc] initWithModels:subarray];
        hub.maxModelsCount = aGroupCount;
        [result addObject:hub];
    }
    
    return result;
}

@end
