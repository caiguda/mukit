//
//  CTCompoundCell.h
//
//
//  Created by Malaar on 11.02.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "MUCell.h"
#import "MUCellDataModeled.h"
#import "MUBOCompoundModel.h"

@class MUTableDisposerModeled;

@interface MUCompoundCell : MUCell
{
    NSMutableArray* subCells;
    NSMutableArray* verticalSeparatorLines;
    
    // for dequeue cells
    NSMutableDictionary *reusableCells;
}

- (UIView*)verticalSeparatorLineAtIndex:(NSUInteger)index;

@end


@interface MUCompoundCellData : MUCellDataModeled

@property (nonatomic, weak) MUTableDisposerModeled* tableDisposer;
@property (nonatomic, readonly) NSMutableArray* cellDatas;

/*
 * spacing between cells
 * by default eq 0
 */
@property (nonatomic, assign) NSUInteger itemSpacing;

/*
 * insets for rectangle which cells will occupy inside CompoundCell
 * UIEdgeInsetsZero by default
 */
@property (nonatomic, assign) UIEdgeInsets itemInsets;

@property (nonatomic, strong) UIColor* verticalSeparatorLinesColor;
@property (nonatomic, assign) BOOL showsVerticalSeparatorLines;     // by default is NO

- (CGFloat)subCellWidthForCompoundCellWidth:(CGFloat)aWidth;

@end