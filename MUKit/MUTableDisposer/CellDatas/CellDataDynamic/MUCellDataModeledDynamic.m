//
//  MUCellDataModeledDynamic.m
//  Pro-Otdyh
//
//  Created by Yuriy Bosov on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MUCellDataModeledDynamic.h"

//==============================================================================
MUCellIndent MUCellIndentMake(float left, float top, float right, float botton)
{
    MUCellIndent cellIndent;
    cellIndent.left = left;
    cellIndent.top = top;
    cellIndent.right = right;
    cellIndent.botton = botton;
    return cellIndent;
}

//==============================================================================
//==============================================================================
//==============================================================================

@implementation MUCellDataModeledDynamic

//==============================================================================
- (id) initWithModel:(id)aModel
{
    self = [super initWithModel:aModel];
    if (self)
    {
        currentCellHeight = 44;
        currentCellWidth = 0;
        cellIndent = [self setupCellIndent];
    }
    return self;
}

//==============================================================================
- (MUCellIndent) setupCellIndent
{
    return MUCellIndentMake(5, 5, 20, 5);
}

//==============================================================================
- (CGFloat) cellHeightForWidth:(CGFloat)aWidth
{
    if ((int)aWidth != currentCellWidth)
    {
        currentCellWidth = (int)aWidth;
        currentCellHeight = [self recalculateCellHeight];
    }
    return currentCellHeight;
}

//==============================================================================
- (CGFloat) recalculateCellHeight
{
    return 44.f;
}

@end
